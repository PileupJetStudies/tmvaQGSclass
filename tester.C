#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TH1F.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
using namespace TMVA;
void tester()
{
   TMVA::Tools::Instance();
 std::map<std::string,int> Use;
   TMVA::Reader *reader = new TMVA::Reader( "!Color:!Silent" );
   Float_t jfullwidth, jnclus;
   Float_t NPV, MU, eta;
   Float_t isHS,isGluon;
   reader->AddVariable( "jfullwidth", &jfullwidth );
   reader->AddVariable( "jnclus", &jnclus );
   reader->AddSpectator( "NPV", &NPV );
   reader->AddSpectator( "MU", &MU );
   reader->AddSpectator( "eta", &eta );
   reader->AddSpectator( "isHS", &isHS );
   reader->AddSpectator( "isGluon", &isGluon );
   TString dir    = "weights/";
   TString prefix = "TMVAMulticlass";
   TString methodName = TString("BDT") + TString(" method");
   TString weightfile = dir + "qgs_BDT.weights.xml";
   reader->BookMVA( methodName, weightfile );

   UInt_t nbin = 100;
   TH2F *histBDTG_gluon  = new TH2F( "MVA_BDTG_gluon",   "MVA_BDTG_gluon",   nbin, 0., 1.1 ,   nbin, 0., 1.1 );
   TH2F *histBDTG_quark  = new TH2F( "MVA_BDTG_quark",   "MVA_BDTG_quark",   nbin, 0., 1.1 ,   nbin, 0., 1.1 );
   TH2F *histBDTG_stoch  = new TH2F( "MVA_BDTG_stoch",   "MVA_BDTG_stoch",   nbin, 0., 1.1 ,   nbin, 0., 1.1 );
   TFile *input(0);
   TString fname = "./qgs.root";
   if (!gSystem->AccessPathName( fname )) {
      input = TFile::Open( fname ); // check if file in local directory exists
   }
   if (!input) {
      std::cout << "ERROR: could not open data file, please generate example data first!" << std::endl;
      exit(1);
   }
   std::cout << "--- TMVAMulticlassApp : Using input file: " << input->GetName() << std::endl;
   TTree* theTree = (TTree*)input->Get("TestTree");
   theTree->SetBranchAddress( "jfullwidth", &jfullwidth );
   theTree->SetBranchAddress( "jnclus", &jnclus );
   theTree->SetBranchAddress( "isHS", &isHS );
   theTree->SetBranchAddress( "isGluon", &isGluon );
   std::cout << "--- Processing: " << theTree->GetEntries() << " events" << std::endl;
   for (Long64_t ievt=0; ievt<theTree->GetEntries();ievt++) {
      if (ievt%1000 == 0){
         std::cout << "--- ... Processing event: " << ievt << std::endl;
      }
      theTree->GetEntry(ievt);
      std::vector<float> output = (reader->EvaluateMulticlass( "BDT method" ));
      if (isHS && isGluon) histBDTG_gluon->Fill(output[1],output[2]);
      if (isHS ** !isGluon) histBDTG_quark->Fill(output[1],output[2]);
      if (!isHS) histBDTG_stoch->Fill(output[1],output[2]);
   }
   TFile *target  = new TFile( "TMVAMulticlassApp.root","RECREATE" );
   histBDTG_gluon->Scale(1./histBDTG_gluon->Integral(0,histBDTG_gluon->GetNbinsX()+1,0,histBDTG_gluon->GetNbinsY()+1));
   histBDTG_quark->Scale(1./histBDTG_quark->Integral(0,histBDTG_quark->GetNbinsX()+1,0,histBDTG_quark->GetNbinsY()+1));
   histBDTG_stoch->Scale(1./histBDTG_stoch->Integral(0,histBDTG_stoch->GetNbinsX()+1,0,histBDTG_stoch->GetNbinsY()+1));
   std::cout << "gluon: gweight=" << histBDTG_gluon->GetMean(1) << ", sweight=" << histBDTG_gluon->GetMean(2) << '\n';
   std::cout << "quark: gweight=" << histBDTG_quark->GetMean(1) << ", sweight=" << histBDTG_quark->GetMean(2) << '\n';
   std::cout << "stoch: gweight=" << histBDTG_stoch->GetMean(1) << ", sweight=" << histBDTG_stoch->GetMean(2) << '\n';
   histBDTG_gluon->Write();
   histBDTG_quark->Write();
   histBDTG_stoch->Write();
   target->Close();
   std::cout << "--- Created root file: \"TMVMulticlassApp.root\" containing the MVA output histograms" << std::endl;
   delete reader;
   std::cout << "==> TMVAClassificationApplication is done!" << std::endl << std::endl;
}
