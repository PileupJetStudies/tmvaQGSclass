#!/usr/bin/env python
# @(#)root/tmva $Id: TMVAClassification.py 38475 2011-03-17 10:46:00Z evt $
# ------------------------------------------------------------------------------ #
import sys    # exit
import time   # time accounting
import getopt # command line parser
import ROOT

# Default settings for command line arguments
DEFAULT_OUTFNAME         = "qgs.root"
DEFAULT_INFNAME          = "/afs/cern.ch/work/m/maklein/calibgit4/optimization/tree.root"
DEFAULT_TREE          = "tree"
#DEFAULT_METHODS          = "kNN100trim,likelihood,BDT"
DEFAULT_METHODS          = "BDT"
DEFAULT_NEVENTS_TEST_S   =0

VARIABLES             = ["jfullwidth","jnclus"]
SELECTION             = "pt>20 && pt<30 && je_sampl0>0.05 && jtime<0.12"
SPECTATORS            = ["NPV", "MU", "eta", "isHS", "isGluon"]

# Print usage help ------------------------------------------------------------------
def usage():
    print " "
    print "Usage: python %s [options]" % sys.argv[0]
    print "  -m | --methods    : gives methods to be run (default: all methods)"
    print "  -i | --inputfile  : name of input ROOT file (default: '%s')" % DEFAULT_INFNAME
    print "  -o | --outputfile : name of output ROOT file containing results (default: '%s')" % DEFAULT_OUTFNAME
    print "  -t | --inputtrees : input ROOT Trees for signal and background (default: '%s')" \
          % (DEFAULT_TREE)
    print "  -v | --verbose"
    print "  -? | --usage      : print this help message"
    print "  -h | --help       : print this help message"
    print " "

# Main routine
def main():
    try:
        # retrive command line options
        shortopts  = "m:i:t:o:vh?"
        longopts   = ["methods=", "inputfile=", "inputtrees=", "outputfile=", "verbose", "help", "usage"]
        opts, args = getopt.getopt( sys.argv[1:], shortopts, longopts )

    except getopt.GetoptError:
        # print help information and exit:
        print "ERROR: unknown options in argument %s" % sys.argv[1:]
        usage()
        sys.exit(1)

    infname     = DEFAULT_INFNAME
    treeName = DEFAULT_TREE
    outfname    = DEFAULT_OUTFNAME
    methods     = DEFAULT_METHODS
    verbose     = False
    for o, a in opts:
        if o in ("-?", "-h", "--help", "--usage"):
            usage()
            sys.exit(0)
        elif o in ("-m", "--methods"):
            methods = a
        elif o in ("-i", "--inputfile"):
            infname = a
        elif o in ("-o", "--outputfile"):
            outfname = a
        elif o in ("-t", "--inputtrees"):
            treeName = a
        elif o in ("-v", "--verbose"):
            verbose = True

    # Print methods
    mlist = methods.replace(' ',',').split(',')
    print "=== TMVAClassification: use method(s)..."
    for m in mlist:
        if m.strip() != '':
            print "=== - <%s>" % m.strip()

    # Import ROOT classes
    from ROOT import gSystem, gROOT, gApplication, TFile, TTree, TCut
    
    # check ROOT version, give alarm if 5.18 
    if gROOT.GetVersionCode() >= 332288 and gROOT.GetVersionCode() < 332544:
        print "*** You are running ROOT version 5.18, which has problems in PyROOT such that TMVA"
        print "*** does not run properly (function calls with enums in the argument are ignored)."
        print "*** Solution: either use CINT or a C++ compiled version (see TMVA/macros or TMVA/examples),"
        print "*** or use another ROOT version (e.g., ROOT 5.19)."
        sys.exit(1)
    
    # Logon not automatically loaded through PyROOT (logon loads TMVA library) load also GUI
    gROOT.SetMacroPath( "./" )
    gROOT.Macro       ( "./TMVAlogon.C" )    
    gROOT.LoadMacro   ( "./TMVAGui.C" )
    
    # Import TMVA classes from ROOT
    from ROOT import TMVA

    # Output file
    outputFile = TFile( outfname, 'RECREATE' )
    
    # Create instance of TMVA factory (see TMVA/macros/TMVAClassification.C for more factory options)
    # All TMVA output can be suppressed by removing the "!" (not) in 
    # front of the "Silent" argument in the option string
#    factory = TMVA.Factory( "TMVAClassification", outputFile, 
#                            "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification" )
    jobname =DEFAULT_OUTFNAME
    factory = TMVA.Factory( jobname.replace(".root", ""), outputFile, 
                            "!V:!Silent:Color:DrawProgressBar:Transformations=I:AnalysisType=multiclass" ) # pascal

    # Set verbosity
    factory.SetVerbose( verbose )

    # Define the input variables that shall be used for the classifier training
    # note that you may also use variable expressions, such as: "3*var1/var2*abs(var3)"
    theCat1Vars = ""; theCat2Vars = ""; theCat3Vars = ""; 
    for var in VARIABLES:
        factory.AddVariable( var, 'F')
        theCat1Vars+=var+":"
        theCat2Vars+=var+":"
        theCat3Vars+=var+":"

    theCat1Vars=theCat1Vars.rstrip(":")
    theCat2Vars=theCat2Vars.rstrip(":")
    theCat3Vars=theCat3Vars.rstrip(":")

    # You can add so-called "Spectator variables", which are not used in the MVA training, 
    for spect in SPECTATORS:
        factory.AddSpectator( spect, spect)

    # open file
    input = TFile.Open( infname )

    # Get the signal and background trees for training
    inputtree      = input.Get( treeName )

    # Global event weights (see below for setting event-wise weights)
    signalWeight     = 1.0
    backgroundWeight = 1.0

    # ====== register trees ====================================================
    factory.AddTree    ( inputtree, "qjet",    signalWeight     )
    factory.AddTree    ( inputtree, "gjet",    signalWeight     )
    factory.AddTree    ( inputtree, "sjet",    signalWeight     )
    factory.AddCut     ( SELECTION+"&&isHS&&!isGluon", "qjet"   )
    factory.AddCut     ( SELECTION+"&&isHS&&isGluon", "gjet"   )
    factory.AddCut     ( SELECTION+"&&!isHS", "sjet"   )

    factory.PrepareTrainingAndTestTree( TCut(""), "SplitMode=Random:NormMode=NumEvents:!V" );

    # multidim likelihood --- kNN
    if "kNN100" in mlist:
        factory.BookMethod( TMVA.Types.kKNN, "KNN100",
                "!V:H:nkNN=100:ScaleFrac=0.8:UseKernel=F:UseWeight=F:Trim=False:BalanceDepth=6" )
    
    if "kNN100trim" in mlist:
        factory.BookMethod( TMVA.Types.kKNN, "KNN100trim",
                "!V:H:nkNN=100:ScaleFrac=0.8:UseKernel=F:UseWeight=F:Trim=True:BalanceDepth=6" )
        
    if "likelihood" in mlist:
        factory.BookMethod( TMVA.Types.kLikelihood, "Likelihood","H:!V:" );
    
    if "BDT" in mlist:
        #BDToptions = "!H:NTrees=850:nEventsMin=150:MaxDepth=5:BoostType=AdaBoost:AdaBoostBeta=0.5:SeparationType=GiniIndex:nCuts=20:PruneMethod=NoPruning:VerbosityLevel=Error"
        BDToptions = "!H:NTrees=850:nEventsMin=150:MaxDepth=5:BoostType=Grad:SeparationType=GiniIndex:nCuts=20:PruneMethod=NoPruning:VerbosityLevel=Error"
        factory.BookMethod( TMVA.Types.kBDT, "BDT",BDToptions)
    
    # ---- Now you can tell the factory to train, test, and evaluate the MVAs. 
    # Train MVAs
    factory.TrainAllMethods()
    
    # Test MVAs
    factory.TestAllMethods()
    
    # Evaluate MVAs
    factory.EvaluateAllMethods()    
    
    # Save the output.
    outputFile.Close()
    
    print "=== wrote root file %s\n" % outfname
    print "=== TMVAClassification is done!\n"
    
    # open the GUI for the result macros    
    gROOT.ProcessLine( "TMVAGui(\"%s\")" % outfname )
    
    # keep the ROOT thread running
    gApplication.Run() 

# ----------------------------------------------------------


if __name__ == "__main__":
    main()
